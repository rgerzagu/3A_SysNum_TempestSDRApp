#!/bin/bash 


# Issue with MESA driver and Ubutu
export LD_PRELOAD=/lib/x86_64-linux-gnu/libstdc++.so.6

# Launch Julia
julia -t auto -O3 --sysimage="./src_tempest/image.so" --project=./src_tempest/TempestSDR/ -L config.jl
