module TempestSDR

# ----------------------------------------------------
# --- Dependencies 
# ---------------------------------------------------- 
using Reexport 
# 
using Distributed 



# ----------------------------------------------------
# --- Dat file managment
# ---------------------------------------------------- 
include("DatBinaryFiles.jl")
@reexport using .DatBinaryFiles
# ----------------------------------------------------
# --- Spectrum 
# ---------------------------------------------------- 
include("GetSpectrum.jl")
@reexport using .GetSpectrum
# ----------------------------------------------------
# --- Demodulation 
# ---------------------------------------------------- 
include("Demodulation.jl")
export amDemod
# ----------------------------------------------------
# --- Resampling methods 
# ---------------------------------------------------- 
include("Resampler.jl")
@reexport using .Resampler
# ----------------------------------------------------
# --- Image renderer
# ---------------------------------------------------- 
include("ScreenRenderer.jl")
@reexport using .ScreenRenderer
# ----------------------------------------------------
# --- Video configurations 
# ---------------------------------------------------- 
include("VideoConfigurations.jl")
# ----------------------------------------------------
# --- Autocorrelation utils
# ---------------------------------------------------- 
include("Autocorrelations.jl")
@reexport using .Autocorrelations
# ----------------------------------------------------
# --- Frame synchronisation 
# ---------------------------------------------------- 
include("FrameSynchronisation.jl")
@reexport using .FrameSynchronisation
# ----------------------------------------------------
# --- Radio in specific core
# ---------------------------------------------------- 
include("ThreadSDRs.jl")
@reexport using .ThreadSDRs
# ----------------------------------------------------
# --- Runtime 
# ---------------------------------------------------- 
#include("Runtime.jl")
#export TempestSDRRuntime
#export init_tempestSDR_runtime
#export plot_findyt
#export plot_findRefresh
#export extract_configuration
#export listener_refresh
#export coreProcessing
#export image_rendering
""" This is a sandbox function to generate an app 
""" 
function julia_main()::Cint 
    include("config.jl")
    # This should import all we need 
    tup = gui(;
	  sdr,
	  carrierFreq,
	  samplingRate,
	  gain,
	  acquisition,
	  kw...
	  )
    return 0
end

include("GUI.jl")
export gui
export start_runtime
export stop_runtime
end
