# Tempest Lab 

## Prequities 

Having Julia (typically 1.8 or 1.9) installed on your machine. Follow the [Julia installation guidelines](https://julialang.org/downloads/)

> This is already done on ENSSAT computers

## Installing the environment 

- Clone this repo on your machine with `git clone https://gitlab.enssat.fr/rgerzagu/3A_SysNum_TempestSDRApp`

- Move to the directory of the cloned repository (with `cd`)
- Install the library and instantiate the Julia environment with `./installTempest.sh`. This may take a while 
- Launch the GUI with `./launchGUI.sh` 

> ❗️ The last command has been specialized for the software environment of ENSSAT and thus may not work if done with other computers.
If you encounter any errors, launch this command in the terminal `julia -t auto -O3 --project=./src_tempest/TempestSDR/ -L config.jl`. This will be a longer to launch the GUI ([as precompilation speeds up everything but is complicated](https://juliatelecom.github.io/TempestSDR.jl/dev/precompilation/)) but it will works.



## Source of the repo 

This repo is a fork of the official [TempestSDR application, done in Julia](https://github.com/JuliaTelecom/TempestSDR.jl)



