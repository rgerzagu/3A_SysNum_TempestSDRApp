#!/bin/bash

# Issue with MESA driver and Ubutu
export LD_PRELOAD=/lib/x86_64-linux-gnu/libstdc++.so.6

# Get the image without compression
cd ./src_tempest/
unzip image.zip

# Instantiate the Julia env 
cd TempestSDR 
julia -t auto -e "using Pkg; Pkg.activate(\".\"); Pkg.instantiate(); using TempestSDR"
cd ..




